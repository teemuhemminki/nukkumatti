
var maximumNumber = 60;



var logStorageName = "kirjuriLogi";

var personStorageName = "kirjuriTilanne";



var numberText = document.getElementById("number");

var warningText = document.getElementById("warning");

var logText = document.getElementById("log");



var addButton = document.getElementById("addButton");

var reduceButton = document.getElementById("reduceButton");

var printButton = document.getElementById("printButton");



var logInUse;

var personInUse;



var log = function(){

	this.events = [];

}



log.prototype.addEvent = function(change, numberInHall){

	var date = new Date();

	var time = date.getDate() +"/"+ date.getMonth() +"/"+ date.getFullYear() + " " + date.getHours() +":"+ date.getMinutes()+":"+ date.getSeconds();

	this.events.push([time, change, numberInHall]);

}



log.prototype.printLog = function(){

	var outPut = "Events";



	for(var i = logInUse.events.length - 1; i >= 0; i --){

		outPut += "<br>Time of event: "+logInUse.events[i][0] +". Event: "+ logInUse.events[i][1] + ". Persons in hall: "+logInUse.events[i][2];

	}

	

	logText.innerHTML = outPut;

}



var persons = function(){

	this.number = 0;

};



var getData = function(name){

	var data = JSON.parse(localStorage.getItem(name));

	return data;

}



var saveData = function(name, data){

	var json = JSON.stringify(data);

	localStorage.setItem(name, json);

}



writeNumber = function(){

	if(personInUse.number > maximumNumber){

		var difference = personInUse.number - maximumNumber;

		warningText.innerHTML = difference+" TOO MANY!";

		warningText.style.color = 'red';

		warningText.style.display = 'block';

	} else if (personInUse.number == maximumNumber){

		warningText.innerHTML = "Maximum number of persons!";	

		warningText.style.color = 'orange';

		warningText.style.display = 'block';

	} else {

		warningText.innerHTML = "";

		warningText.style.display = 'none';

	}

	numberText.innerHTML = "Persons in hall :" + personInUse.number + "/" + maximumNumber;

}



personInUse = new persons();



if(localStorage.getItem(personStorageName) != null){

	personInUse.number = getData(personStorageName).number;

}



logInUse = new log();

	

if(localStorage.getItem(logStorageName) != null){

	logInUse.events = getData(logStorageName).events;

}



addPerson = function(){

	personInUse.number ++;

	writeNumber();

	logInUse.addEvent("Addition", personInUse.number);

	saveData(personStorageName, personInUse);

	saveData(logStorageName, logInUse);

	logInUse.printLog();

}



reducePerson = function(){

	if(personInUse.number > 0){

		personInUse.number --;

		logInUse.addEvent("Substraction", personInUse.number);

		writeNumber();

	}

	saveData(personStorageName, personInUse);

	saveData(logStorageName, logInUse);

	logInUse.printLog();	

}



addButton.onclick = function(){

	addPerson();

}



reduceButton.onclick = function(){

	reducePerson();

}



writeNumber();

logInUse.printLog();
